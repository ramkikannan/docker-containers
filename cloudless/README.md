This docker container detects cloud in an 512x512 RGB PNG image . This container is built on travis johnston caffe docker container for cpu(travisjohnston/caffe) and his docker file can be found at https://github.com/jtjohnston/containers/tree/master/caffe. We use the cloudless https://github.com/BradNeuberg/cloudless model and run the cnn classifier based on this prebuild model. The details about cloudless can be found at http://codinginparadise.org/ebooks/html/blog/introducing_cloudless.html 

To run the container execute "docker run -it ramkikannan/cloudless bash"

If you get the error "libdc1394 error: Failed to initialize libdc1394", running "ln /dev/null /dev/raw1394" will alleviate the error.

In the ./src/cloudless/train/predict.py file, change caffe.set_mode_gpu() to caffe.set_mode_cpu() under _initialize_caffe function and change to "training_mean_pickle = os.path.abspath(args["training_mean_pickle"])". Keep the changed predict.py under the Dockerfile directory.  

To run the classifier execute the following command from inside $CAFFE_ROOT/cloudless

./src/cloudless/train/predict.py --image examples/cloud.png --training_mean_pickle=/root/caffe/data/ilsvrc12/imagenet_mean.npy --input_weight_file=/root/caffe/cloudless/src/caffe_model/bvlc_alexnet/bvlc_alexnet_finetune.caffemodel 
